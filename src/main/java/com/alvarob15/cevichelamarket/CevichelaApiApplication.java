package com.alvarob15.cevichelamarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CevichelaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CevichelaApiApplication.class, args);
    }

}
